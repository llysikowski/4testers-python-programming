from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print("Hello there!", i)


def print_positive_numbers_in_range(start, stop):
    for i in range(start, stop + 1):
        print(i)


def print_positive_numbers_in_range_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(n):
        print(randint(1, 1000))


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_square_roots_of_numbers(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        square_roots.append(round(sqrt(number), 2))
    return square_roots


# function converting list of temperatures in Celsius to list of temperatures in Fahrenheits
def convert_list_of_temperatures_in_celsius_to_fahrenheit(list_of_temperature_in_celsius):
    list_of_temperature_in_fahrenheit = []
    for temp in list_of_temperature_in_celsius:
        list_of_temperature_in_fahrenheit.append(temp * 1.8 + 32)
    return list_of_temperature_in_fahrenheit


if __name__ == '__main__':
    # print_hello_40_times()
    # print_positive_numbers_in_range(10, 20)
    # print_positive_numbers_in_range_divisible_by_7(20, 80)

    print_n_random_numbers(5)

    list_of_measurement_results = [11.0, 123, 69, 21, 37, 1, 5]
    print_square_roots_of_numbers(list_of_measurement_results)
    print(get_square_roots_of_numbers(list_of_measurement_results))

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(convert_list_of_temperatures_in_celsius_to_fahrenheit(temps_celsius))
