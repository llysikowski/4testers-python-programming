# Function displaying speed information
def display_speed_information(speed):
    if speed > 50:
        print(f"Slow down! Speed {speed} is too high!")
    else:
        print(f"Speed {speed} is below limit! Thank you!")


# Function checking if conditions are normal
def check_if_conditions_are_normal(temperature_in_celsius, pressure_in_hectopascals):
    return True if temperature_in_celsius == 0 and pressure_in_hectopascals == 1013 else False


# Function printing the value of fines for speed in built-up area
def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print(f"Your speed was: {speed}.")
    if speed > 100:
        print("You just lost your driving license!")
    elif speed > 75:
        print("You must pay 500 PLN!")
    elif speed > 50:
        print("You must pay 200 PLN!")
    else:
        print("Thanks! Your speed is fine!")


def describe_the_college_grade(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):
        return "N/A"
    if grade < 2 or grade > 5:
        return "N/A"
    elif grade >= 4.5:
        return "Bardzo dobry"
    elif grade >= 4:
        return "Dobry"
    elif grade >= 3:
        return "Dostateczny"
    else:
        return "Niedostateczny"


if __name__ == '__main__':
    print(describe_the_college_grade(2))
    print(describe_the_college_grade(3))
    print(describe_the_college_grade(4))
    print(describe_the_college_grade(4.5))
    print(describe_the_college_grade(-100))
    print(describe_the_college_grade("Grade"))


    # print_the_value_of_speeding_fine_in_built_up_area(101)
    # print_the_value_of_speeding_fine_in_built_up_area(100)
    # print_the_value_of_speeding_fine_in_built_up_area(76)
    # print_the_value_of_speeding_fine_in_built_up_area(75)
    # print_the_value_of_speeding_fine_in_built_up_area(51)
    # print_the_value_of_speeding_fine_in_built_up_area(50)

    # print(check_if_conditions_are_normal(0,1013))
    # print(check_if_conditions_are_normal(1,1013))
    # print(check_if_conditions_are_normal(0,1014))
    # print(check_if_conditions_are_normal(1,1014))

    # first_car_speed = 50
    # second_car_speed = 51
    # third_car_speed = 49
    # display_speed_information(first_car_speed)
    # display_speed_information(second_car_speed)
    # display_speed_information(third_car_speed)
