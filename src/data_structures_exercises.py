import string
import random


# Function returning the sum of the numbers from the list
def calculate_average_of_list_numbers(list_of_numbers):
    return sum(list_of_numbers) / len(list_of_numbers)


# Function returning the average of two numbers
def calculate_average_of_two_numbers(number_1, number_2):
    return (number_1 + number_2) / 2


# Get random password pf length 8 with letters, digits, and symbols
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


if __name__ == '__main__':
    january = [-4, 1, -7, 2]
    february = [-13, -9, -3, 3]
    print(
        f"Average temperature in the last two months is {calculate_average_of_two_numbers(calculate_average_of_list_numbers(january), calculate_average_of_list_numbers(february))}")

    print(generate_login_data("andor@starwars.com"))
    print(generate_login_data("mando@starwars.com"))
    print(generate_login_data("kenobi@starwars.com"))
