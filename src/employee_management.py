import random
import string


# Function generating random email address
def generate_random_email():
    source = string.ascii_letters + string.digits
    return ''.join((random.choice(source) for _ in range(random.randrange(5, 15)))) + "@example.com"


# Function generating random seniority years
def generate_seniority_years():
    return random.randint(5, 20)


# Function generating random boolean
def generate_random_boolean():
    return random.choice([False, True])


# Function returning random employee data
def generate_employee_data():
    return {"email": generate_random_email(), "seniority_years": generate_seniority_years(),
            "female": generate_random_boolean()}


# Functon returning the list of employee data
def generate_list_of_random_employees_data(list_length):
    list_of_employees_data = []
    for i in range(list_length):
        list_of_employees_data.append(generate_employee_data())
    return list_of_employees_data


# Function returning the list of emails of all employee's with seniority greater than 10
def get_emails_of_employees_with_seniority_over_10_years(employees):
    list_of_emails = []
    for employee in employees:
        if employee["seniority_years"] > 10:
            list_of_emails.append(employee["email"])
    return list_of_emails


# Function returning the list of women employee data
def get_women_employee_data(employees):
    list_of_women = []
    for employee in employees:
        if employee["female"]:
            list_of_women.append(employee)
    return list_of_women


if __name__ == '__main__':
    list_of_employees = generate_list_of_random_employees_data(5)
    print(list_of_employees)
    print(get_emails_of_employees_with_seniority_over_10_years(list_of_employees))
    print(get_women_employee_data(list_of_employees))
