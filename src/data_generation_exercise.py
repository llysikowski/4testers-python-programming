import random
from datetime import date


# Lists with data to be randomized
female_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
min_age = 5
max_age = 45


# Function generating the email address for the given name and surname
def generate_email_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@example.com"


# Function generating random age
def generate_random_age():
    return random.randint(min_age, max_age)


# Function checking if person is an adult
def is_person_an_adult(age):
    return True if age >= 18 else False


# Function calculating the year of birth based on person's age
def calculate_the_birth_year(age):
    return date.today().year - age


# Function generating random person data
def generate_person_data(female):
    person_name = random.choice(female_names) if female else random.choice(male_names)
    person_surname = random.choice(surnames)
    person_age = generate_random_age()
    return {
        "firstname": person_name,
        "lastname": person_surname,
        "country": random.choice(countries),
        "email": generate_email_address(person_name, person_surname),
        "age": person_age,
        "adult": is_person_an_adult(person_age),
        "birth_year": calculate_the_birth_year(person_age)
    }


# Function generating list of people depending on two counters - how many man and woman should be added to the list
def generate_list_of_people(man_counter, woman_counter):
    list_of_people = []
    for i in range(man_counter):
        list_of_people.append(generate_person_data(False))
    for i in range(woman_counter):
        list_of_people.append(generate_person_data(True))
    return list_of_people


# Function generating and printing a welcome message for all people in the given list
def generate_and_print_list_of_people_welcome_message(people_list):
    for person in people_list:
        print(
            f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}.")


if __name__ == '__main__':
    # Generate list of 10 people - 5 man and 5 woman
    people = generate_list_of_people(5, 5)
    # Print list of people from the generated list
    print(people)
    # Print welcome messages for all people in the generated list
    generate_and_print_list_of_people_welcome_message(people)
