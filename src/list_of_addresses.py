addresses = [
    {'city': 'New York', 'street': '5th Avenue', 'house_number': '10', 'post_code': '10-200'},
    {'city': 'Seattle', 'street': 'Washington Street', 'house_number': '25A', 'post_code': '20-510'},
    {'city': 'San Francisco', 'street': 'Bay Area Street', 'house_number': '30', 'post_code': '15-000'}
]

print(f"Post code of the last address is: {addresses[-1]['post_code']}")
print(f"City of the second address is: {addresses[1]['city']}")

addresses[0]['street'] = '10th Avenue'

print(addresses)
