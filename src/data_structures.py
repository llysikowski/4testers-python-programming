# lists
movies = ['Rogue One', 'A New Hope', 'Interstellar', 'Coach Carter', 'Empire Strikes Back']

last_movie = movies[-1]
movies.append('Lord of the Rings')
movies.append('Harry Potter')

print(len(movies))

middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'The Phantom Menace')
print(movies)

emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')
print(emails)

# dictionaries
friend = {
    "name": "Csssian",
    "age": 35,
    "hobbies": ["Escaping from prisons", "Killing Stormtroopers"]
}

friend_hobbies = friend["hobbies"]
print("Hobbies of my friend: ", friend_hobbies)

friend["hobbies"].append("Flirting with Bix")
print("Hobbies of my friend: ", friend_hobbies)

# adding a new key to dictionary
friend["married"] = False
