animals = [
    {'name': 'Chewie', 'kind': 'dog', 'age': 14},
    {'name': 'Fel', 'kind': 'hamster', 'age': None},
    {'name': 'Domino', 'kind': 'horse', 'age': 8}
]

print(animals[-1]['name'])
animals[1]['age'] = 2  # assign value to the dictionary key
print(animals[1])
animals.insert(0, {'name': 'Rex', 'kind': 'dinosaur', 'age': 120})
print(animals)
