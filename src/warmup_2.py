# function calculation square of the given number
def calculate_square_of_the_number(input_number):
    return input_number ** 2


# function converting temperature in Celsius to temperature in Fahrenheits
def convert_celsius_to_fahrenheit(temperature_in_celsius):
    return temperature_in_celsius * 1.8 + 32


# function calculating volume of a cuboid
def calculate_volume_of_a_cuboid(side_a, side_b, side_c):
    return side_a * side_b * side_c


if __name__ == '__main__':
    print(calculate_square_of_the_number(0))
    print(calculate_square_of_the_number(16))
    print(calculate_square_of_the_number(2.55))

    first_temperature = 20
    print(f"{first_temperature} in Celsius equals {convert_celsius_to_fahrenheit(first_temperature)} in Fahrenheit.")

    cuboid_side_a = 3
    cuboid_side_b = 5
    cuboid_side_c = 7
    print(
        f"Volume of cuboid with sides {cuboid_side_a}, {cuboid_side_b} and {cuboid_side_c} equals {calculate_volume_of_a_cuboid(cuboid_side_a, cuboid_side_b, cuboid_side_c)}.")
