first_name = "Lukasz"
last_name = "Lysikowski"
email = "lysikolukasz@gmail.com"

my_bio = "Mam na imie " + first_name + ". Moje nazwisko to " + last_name + ". Moj email to " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imie {first_name}.\nMoje nazwisko to {last_name}.\nMoj email to {email}."
print(my_bio_using_f_string)

# Algebra
circle_radius = 5
area_of_a_circle = 3.14 * circle_radius ** 2
circumference_of_a_circle = 2 * 3.14 * circle_radius

print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle)
print(f"Area of a circle with radius {circle_radius}:", circumference_of_a_circle)
