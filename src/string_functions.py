# Function printing the town welcome message
def print_welcome_message(person_name, city):
    print(f"Hello {person_name}! Nice to see you in the city of {city}!")


# Function generating email addresses in the 4testers company's domain
def generate_company_email_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"


if __name__ == '__main__':
    print(generate_company_email_address("Janusz", "Nowak"))
    print(generate_company_email_address("Barbara", "Kowalska"))

    # print_welcome_message("Michael", "Scranton")
    # print_welcome_message("Jim", "Philadelphia")
