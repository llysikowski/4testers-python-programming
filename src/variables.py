first_name = "Lukasz"
last_name = "Lysikowski"
age = 31

print(first_name)
print(last_name)
print(age)

# I amd defining a set of variables describing my friend
friends_name = "Grzegorz"
friends_age = 32
friends_number_of_animals = 0
friends_driving_license = True
friendship_duration_in_years = 10.5

print(friends_name, friends_age, friends_number_of_animals, friends_driving_license, friendship_duration_in_years, sep=', ')
